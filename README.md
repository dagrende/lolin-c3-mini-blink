# Wemos Lolin c3 mini Blink

## Prerequisites

* install vs code
* install extension PlatformIO

## Install and run app first time

* git clone https://gitlab.com/dagrende/lolin-c3-mini-blink.git
* cd lolin-c3-mini-blink
* code .
* click checkmark to build (bottom status bar)
* connect c3-mini board to usb
* hold 9 button on board pressed
* press and release rst button
* release 9 button
* upload by clicking right arrow on bottom bar
* when upload ready, press-release rst button
* blue led should blink slowly

After this you can change the program and upload by just clicking the right arrow button. You don't hav to push the 9 or rst buttons.

Wemos information page abouit the c3-mini: https://www.wemos.cc/en/latest/c3/c3_mini.html.